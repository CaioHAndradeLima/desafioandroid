package caiohenrique.aplicacao.resources_http;

import android.app.Activity;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import java.io.InputStream;
import java.net.URL;

import caiohenrique.aplicacao.Repositorio;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Caio on 08/07/2017.
 * public class ServiceBroadcast extends IntentService {


 public ServiceBroadcast() {
 super("ServiceEvents");
 }

 @Override
 protected void onHandleIntent(@Nullable Intent intent) {

 AuxiliarBanco auxBD = new AuxiliarBanco(getApplication());
 List<ContatoTelefonico> ListaContatoTelefonico = ListaTelefonica.ListaContatosPhone(getApplicationContext());

 List<Amigo> ListAmigos = Requisicoes.SendNumbersUserForWebService(ListaContatoTelefonico);

 if(ListAmigos != null) {

 for (Amigo listAmigo : ListAmigos) {

 auxBD.insereAmigo(listAmigo);
 //Código para tentar upar a foto do usuário deve ser inserido aqui...
 }

 }

 }
 }
 */

public class DownloadImagem  extends AsyncTask<String, Void, Bitmap> {

    CircleImageView imagem;
    Repositorio repositorio;
    Context mContext;


    public DownloadImagem(CircleImageView imagem, Repositorio repositorio, Context context) {
        this.imagem = imagem;
        this.repositorio = repositorio;
        this.mContext = context;
    }


    @Override
    protected Bitmap doInBackground(String... params) {

        Bitmap bitmap = null;

        try {
            InputStream in = new URL(repositorio.Dono.getAvatar_url()).openStream();
            bitmap = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        repositorio.Dono.setImageBitmap(bitmap,mContext);
        imagem.setImageBitmap(repositorio.Dono.getBitmap());
    }
}

