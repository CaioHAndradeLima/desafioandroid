package caiohenrique.aplicacao.resources_http;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.InputStream;
import java.net.URL;

import caiohenrique.aplicacao.Pull_Request;
import caiohenrique.aplicacao.Repositorio;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Caio on 08/07/2017.
 */

public class DownloadImagemPull  extends AsyncTask<String, Void, Bitmap> {

    CircleImageView imagem;
    Pull_Request pullRequest;
    Context mContext;


    public DownloadImagemPull(CircleImageView imagem, Pull_Request pullRequest, Context context) {
        this.imagem = imagem;
        this.pullRequest = pullRequest;
        this.mContext = context;
    }


    @Override
    protected Bitmap doInBackground(String... params) {

        Bitmap bitmap = null;

        try {
            InputStream in = new URL(pullRequest.user.getAvatar_url()).openStream();
            bitmap = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        pullRequest.user.setImageBitmap(bitmap,mContext);
        imagem.setImageBitmap(pullRequest.user.getBitmap());
    }
}
