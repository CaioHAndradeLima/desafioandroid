package caiohenrique.aplicacao.resources_http;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.List;

import caiohenrique.aplicacao.ActivityPullRequest;
import caiohenrique.aplicacao.MainActivity;
import caiohenrique.aplicacao.Pull_Request;
import caiohenrique.aplicacao.Repositorio;
import caiohenrique.aplicacao.RepositorioAdapter;

/**
 * Created by Caio on 08/07/2017.
 */

public class GerenciadorHttp {



    public static final String UrlBasica = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=";
    public static final String UrlBasicaPulls = "https://api.github.com/repos/";

    public static String BuildUrl(String name_defualt) {
        return UrlBasicaPulls + name_defualt + "/pulls";
    }
    public static List<Repositorio> RequerendoRepositorios(int page) {


        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        List<Repositorio> ListaDeRepositorios = null;
  /*          SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
            String ordem = preferences.getString(getString(R.string.prefs_ordem_key), "");
            String idioma = preferences.getString(getString(R.string.prefs_idioma_key), "");
*/
        try {
            String urlBase = UrlBasica + page;


            URL url = new URL(urlBase);
            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.setRequestProperty("User-Agent", "Mozilla/5.0");
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            InputStream inputStream = urlConnection.getInputStream();
            if (inputStream == null) {
                return null;
            }

            reader = new BufferedReader(new InputStreamReader(inputStream));

            String linha;
            StringBuffer buffer = new StringBuffer();
            while ((linha = reader.readLine()) != null) {
                buffer.append(linha);
            }

            ListaDeRepositorios = JsonUtil.fromJsonToListRepositories(buffer.toString());


        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            try {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    reader.close();

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return ListaDeRepositorios;

    }

    public static List<Pull_Request> RequerendoPulls(String name_default) {


        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        List<Pull_Request> ListaDePulls = null;
  /*          SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
            String ordem = preferences.getString(getString(R.string.prefs_ordem_key), "");
            String idioma = preferences.getString(getString(R.string.prefs_idioma_key), "");
*/
        try {
            String urlBase = BuildUrl(name_default);


            URL url = new URL(urlBase);
            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.setRequestProperty("User-Agent", "Mozilla/5.0");
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            InputStream inputStream = urlConnection.getInputStream();
            if (inputStream == null) {
                return null;
            }

            reader = new BufferedReader(new InputStreamReader(inputStream));

            String linha;
            StringBuffer buffer = new StringBuffer();
            while ((linha = reader.readLine()) != null) {
                buffer.append(linha);
            }

            if(buffer.toString().length() < 100) return null;
            ListaDePulls = JsonUtil.fromJsonToListPullRequest(buffer.toString());


        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            try {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    reader.close();

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return ListaDePulls;

    }


    public static void CarregandoPullRequest(final ActivityPullRequest activityPullRequest, final String name_pull_request) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                final List<Pull_Request> ListaDeNovosPulls = RequerendoPulls(name_pull_request);

                activityPullRequest.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(ListaDeNovosPulls == null) {
                            Toast.makeText(activityPullRequest,"Não houve conexão! ", Toast.LENGTH_LONG).show();
                        } else {
                            activityPullRequest.SetandoAdapter(ListaDeNovosPulls);
                        }
                    }
                });
            }
        }).start();

    }

    public static void GerenciandoNovosRepositorios(final MainActivity mainActivity, final int pagina) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                boolean verifica = false;
                final List<Repositorio> NovosRepositorios = RequerendoRepositorios(pagina);

                if (NovosRepositorios == null) {
                    verifica = true;
                }

                final boolean verifica1 = verifica;
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (verifica1) {
                            Toast.makeText(mainActivity, "Verifique sua conexão...", Toast.LENGTH_LONG).show();
                        } else {
                            mainActivity.AdicionandoRepositorios(NovosRepositorios);
                        }
                    }
                });
            }
        }).start();

    }


    public static File CriarFileBitmap(Context activity, String nameArquivo, Bitmap bitmap) {
        File file = new File(activity.getCacheDir(), nameArquivo);
        //Name com o tipo
        //Neste momento /\ crio um cache no package do app para a imagem
        try {
            //create a file to write bitmap data
            //Convert bitmap to byte array
            Bitmap mbitmap = bitmap;
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            mbitmap.compress(Bitmap.CompressFormat.JPEG, 100 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();

            //write the bytes in file
            FileOutputStream fos = null;

            fos = new FileOutputStream(file);

            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    public static void GerenciandoNovosRepositorios(final int pagina, final MainActivity mainActivity) {

        final int paginanew = pagina;
        new Thread(new Runnable() {
            @Override
            public void run() {
                final List<Repositorio> ListaDeNovosRepositorios = RequerendoRepositorios(paginanew);

                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(ListaDeNovosRepositorios == null) {
                            Toast.makeText(mainActivity,"Não houve conexão! ", Toast.LENGTH_LONG).show();
                        } else {
                            mainActivity.SetandoAdapter(ListaDeNovosRepositorios);
                        }
                    }
                });
            }
        }).start();

    }

    public static void realizarDownloadEmBitmap(RepositorioAdapter.MyViewHolder myViewHolder, Repositorio repositorio) {

    }
}
