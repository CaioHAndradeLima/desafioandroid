package caiohenrique.aplicacao.resources_http;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import caiohenrique.aplicacao.Pull_Request;
import caiohenrique.aplicacao.Repositorio;
import caiohenrique.aplicacao.owner;

/**
 * Created by Caio on 08/07/2017.
 */

public class JsonUtil {

    public static List<Repositorio> fromJsonToListRepositories(String json) {
        List<Repositorio> listaDeRepositorios = new ArrayList<>();
        try {
            JSONObject jsonBase = new JSONObject(json);
            JSONArray results = jsonBase.getJSONArray("items");

            for (int i = 0; i < results.length(); i++) {
                JSONObject object = results.getJSONObject(i);
                JSONObject JsonObjetcOwner = object.getJSONObject("owner");
                Repositorio repositorio = new Repositorio(object, JsonObjetcOwner);
                listaDeRepositorios.add(repositorio);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return listaDeRepositorios;
    }


    public static List<Pull_Request> fromJsonToListPullRequest(String json) {
        List<Pull_Request> listaPulls = new ArrayList<>();
        try {
            JSONArray results = new JSONArray(json);

            for (int i = 0; i < results.length(); i++) {
                JSONObject object = results.getJSONObject(i);
                JSONObject JsonObjetcUser = object.getJSONObject("user");
                Pull_Request Pull = new Pull_Request(object, JsonObjetcUser);
                listaPulls.add(Pull);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return listaPulls;
    }

}
