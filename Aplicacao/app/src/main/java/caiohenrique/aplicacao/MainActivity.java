package caiohenrique.aplicacao;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import caiohenrique.aplicacao.cache.AuxiliarBanco;
import caiohenrique.aplicacao.resources_http.GerenciadorHttp;

public class MainActivity extends AppCompatActivity implements RepositorioAdapter.CliqueItem {

    RecyclerView mRecyclerView;
    List<Repositorio> mList;
    int pagina = 1;
    RepositorioAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = (RecyclerView) findViewById(R.id.activity_main_recyclerview);
        mRecyclerView.setHasFixedSize(true); //diz que o recycler nao vai mudar

        mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                //quando o scroll chega no final
                if (adapter == null) return;
                LinearLayoutManager llm = (LinearLayoutManager) mRecyclerView.getLayoutManager();
                adapter = (RepositorioAdapter) mRecyclerView.getAdapter();
                // ele recupera os objetos para poder adicionar mais reclyceview
                if (mList.size() == llm.findLastCompletelyVisibleItemPosition() + 1) {
                    //Comparação do tamanho da lista se esta no último objeto (+1 pois size começa em 1)
                    GerenciadorHttp.GerenciandoNovosRepositorios(MainActivity.this, pagina);
                }
            }
        });


        //Crio o LinearLayoutManager
        LinearLayoutManager llm = new LinearLayoutManager(MainActivity.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL); //seto sua horientação
        mRecyclerView.setLayoutManager(llm); ///seto o layout no recyclerview


        AuxiliarBanco auxBD = new AuxiliarBanco(this);
        try {
            List<Repositorio> ListaDeRepositoriosBD = auxBD.BuscaTodosRepositorios();

            if (ListaDeRepositoriosBD != null) {
                SetandoAdapter(ListaDeRepositoriosBD);
            } else {
                GerenciadorHttp.GerenciandoNovosRepositorios(pagina, MainActivity.this);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void SetandoAdapter(List<Repositorio> ListaNova) {

        mList = new ArrayList<>();
        for (Repositorio repositorio : ListaNova) {
            mList.add(repositorio);
            //adapter.addListItem(listAux.get(i), mList.size());
        }
        adapter = new RepositorioAdapter(MainActivity.this, mList, MainActivity.this);
        mRecyclerView.setAdapter(adapter);
        pagina++;
    }

    public void AdicionandoRepositorios(List<Repositorio> ListaNova) {
        AuxiliarBanco auxBD = new AuxiliarBanco(this);
        for (Repositorio repositorio : ListaNova) {
            adapter.addListItem(repositorio, mList.size());
            auxBD.insereUsuario(repositorio);
        }
        pagina++;
    }

    @Override
    public void Clique(int position) {
        Intent it = new Intent(this, ActivityPullRequest.class);
        it.putExtra(KEY_REPOSITORIO, mList.get(position).getFull_name());
        startActivity(it);
    }

    public static final String KEY_REPOSITORIO = "KEYREPOSITORIE";
}
