package caiohenrique.aplicacao;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;
import caiohenrique.aplicacao.resources_http.DownloadImagemPull;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Caio on 08/07/2017.
 */

public class PullRequestAdapter  extends RecyclerView.Adapter<PullRequestAdapter.MyViewHolder> {
    private List<Pull_Request> mList;
    private LayoutInflater mLayoutInflater;
    private Context context;


    public PullRequestAdapter(Context c, List<Pull_Request> lista){
        mList = lista;
        mLayoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = c;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = mLayoutInflater.inflate(R.layout.layout_lista_pulls, viewGroup, false);
        MyViewHolder mvh = new MyViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, int position) {
        Log.i("LOG", "onBindViewHolder()");
        myViewHolder.titulo.setText(mList.get(position).getTitle() );
        myViewHolder.nome_criador.setText( mList.get(position).user.getLogin() );
        myViewHolder.data.setText(mList.get(position).getUpdated_at());
        myViewHolder.descricao.setText(mList.get(position).getBody());

        if(mList.get(position).user.getBitmap() == null) {
            new DownloadImagemPull(myViewHolder.imagem_perfil,mList.get(position),context).execute();
        } else {
            myViewHolder.imagem_perfil.setImageBitmap(mList.get(position).user.getBitmap());
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    public void addListItem(Pull_Request c, int position){
        mList.add(c);
        notifyItemInserted(position);
    }


    public void removeListItem(int position){
        mList.remove(position);
        notifyItemRemoved(position);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public CircleImageView imagem_perfil;
        public TextView titulo, nome_criador, data, descricao;

        public MyViewHolder(View itemView) {
            super(itemView);
            imagem_perfil = itemView.findViewById(R.id.layout_lista_pulls_CircleImageView);
            titulo = itemView.findViewById(R.id.layout_lista_pulls_titulo);
            nome_criador = itemView.findViewById(R.id.layout_lista_pulls_Name_Usuario);
            data = itemView.findViewById(R.id.layout_lista_pulls_data);
            descricao = itemView.findViewById(R.id.layout_lista_pulls_Descricao);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

        }
    }


}
