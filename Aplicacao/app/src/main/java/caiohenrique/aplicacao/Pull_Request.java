package caiohenrique.aplicacao;

import android.content.Context;
import android.graphics.Bitmap;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import caiohenrique.aplicacao.resources_http.GerenciadorHttp;

/**
 * Created by Caio on 08/07/2017.
 */

public class Pull_Request {

    private String title, updated_at, url, body;
    public User user;

    public Pull_Request(JSONObject jsonObject , JSONObject JsonObjUser) throws JSONException {

        this.title           = jsonObject.getString("title");
        this.updated_at      = jsonObject.getString("updated_at");
        this.url             = jsonObject.getString("url");
        this.body            = jsonObject.getString("body");

        this.user = new User();
        this.user.setAvatar_url(JsonObjUser.getString("avatar_url"));
        this.user.setLogin(JsonObjUser.getString("login"));

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }


    public class User {
        private String avatar_url, login;
        private Bitmap bitmap;

        public String getAvatar_url() {
            return avatar_url;
        }

        public void setAvatar_url(String avatar_url) {
            this.avatar_url = avatar_url;
        }

        public String getLogin() {
            return login;
        }

        public void setLogin(String login) {
            this.login = login;
        }

        public void setImageBitmap(Bitmap bitmap, Context activity) {
            File file1 = GerenciadorHttp.CriarFileBitmap(activity, "imagemcortada.jpg", bitmap);
            this.bitmap = owner.decodeFileAndRecort(file1, 250);
        }

        public Bitmap getBitmap() {
            return this.bitmap;
        }



    }

}
