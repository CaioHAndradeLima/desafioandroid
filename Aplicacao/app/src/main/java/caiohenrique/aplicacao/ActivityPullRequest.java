package caiohenrique.aplicacao;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import caiohenrique.aplicacao.resources_http.GerenciadorHttp;

public class ActivityPullRequest extends AppCompatActivity {

    RecyclerView mRecyclerView;
    List<Pull_Request> mList;
    PullRequestAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = (RecyclerView) findViewById(R.id.activity_main_recyclerview);
        mRecyclerView.setHasFixedSize(true); //diz que o recycler nao vai mudar

        mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                //quando o scroll chega no final
                if(adapter == null) return;
                LinearLayoutManager llm = (LinearLayoutManager) mRecyclerView.getLayoutManager();
                adapter = (PullRequestAdapter) mRecyclerView.getAdapter();
                // ele recupera os objetos para poder adicionar mais reclyceview
                if (mList.size() == llm.findLastCompletelyVisibleItemPosition() + 1) {
                    //Comparação do tamanho da lista se esta no último objeto (+1 pois size começa em 1)
                //    GerenciadorHttp.GerenciandoNovosRepositorios( Ac.this, pagina);

                }
            }
        });


        //Crio o LinearLayoutManager
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL); //seto sua horientação
        mRecyclerView.setLayoutManager(llm); ///seto o layout no recyclerview

        Intent it = getIntent();

        GerenciadorHttp.CarregandoPullRequest(this, it.getStringExtra(MainActivity.KEY_REPOSITORIO));
        // mRecyclerView.setAdapter( adapter ); //seto no reclycerview


    }

    public void SetandoAdapter(List<Pull_Request> ListaNova) {

        mList = new ArrayList<>();
        for (Pull_Request pull : ListaNova) {
            mList.add(pull);
            //adapter.addListItem(listAux.get(i), mList.size());
        }
        adapter = new PullRequestAdapter(this, mList);
        mRecyclerView.setAdapter( adapter );
    }

}
