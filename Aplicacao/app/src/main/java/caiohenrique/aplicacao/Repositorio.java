package caiohenrique.aplicacao;

import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Caio on 08/07/2017.
 */

    public class Repositorio implements Serializable {
    private String name, full_name, description,  forks;
    public owner Dono;
    private int id;

    public Repositorio() { }

    public Repositorio(JSONObject jsonObject , JSONObject JsonObjUser) throws JSONException {

        this.name           = jsonObject.getString("name");
        this.full_name      = jsonObject.getString("full_name");
        this.description    = jsonObject.getString("description");
        this.forks          = jsonObject.getString("forks");

        this.Dono = new owner();
        this.Dono.setAvatar_url(JsonObjUser.getString("avatar_url"));
        this.Dono.setId(JsonObjUser.getString("id"));
        this.Dono.setLogin(JsonObjUser.getString("login"));

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getForks() {
        return forks;
    }

    public void setForks(String forks) {
        this.forks = forks;
    }

    public owner getDono() {
        return Dono;
    }

    public void setDono(owner dono) {
        Dono = dono;
    }
}
