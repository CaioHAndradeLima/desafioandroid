package caiohenrique.aplicacao.cache;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Caio on 09/07/2017.
 */

public class CreateDB extends SQLiteOpenHelper {


    private static final String NOME_BANCO = "bancodedados";
    private static final int VERSAO = 1;

    public CreateDB(Context context) {
        super(context, NOME_BANCO, null, VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(returnStringTableCache());

    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + ScriptDeTabela.NAMETABLE);
        onCreate(db);
    }

    public String returnStringTableCache() {
        ScriptDeTabela pt = new ScriptDeTabela();

        pt.CreateTable(ScriptDeTabela.NAMETABLE);
        pt.Add_Atributo(ScriptDeTabela.id_0, " integer primary key autoincrement");
        pt.Add_Atributo(ScriptDeTabela.nome_1, "text");
        pt.Add_Atributo(ScriptDeTabela.full_name_2, "text");
        pt.Add_Atributo(ScriptDeTabela.Description_3, "text");
        pt.Add_Atributo(ScriptDeTabela.Forks_4, "text");
        pt.Add_Atributo(ScriptDeTabela.Owner_Login_5, "text");
        pt.Add_Atributo(ScriptDeTabela.Owner_COD_6, "text");
        pt.Add_Atributo(ScriptDeTabela.Owner_AvatarURL_7, "text");
        return pt.Build();
    }
}