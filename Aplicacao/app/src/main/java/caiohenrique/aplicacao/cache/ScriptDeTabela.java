package caiohenrique.aplicacao.cache;

/**
 * Created by Caio on 09/07/2017.
 */

public class ScriptDeTabela {

        public static final String NAMETABLE        = "cache";
        public static final String id_0             = "id";
        public static final String nome_1           = "nome";
        public static final String full_name_2      = "foto";
        public static final String Description_3    = "DESCRIPTION";
        public static final String Forks_4          = "NumberForks";
        public static final String Owner_Login_5    = "OwnerLogin";
        public static final String Owner_COD_6      = "OwnerCod";
        public static final String Owner_AvatarURL_7= "OwnerAvatarUrl";


    public void CreateTable(String NomeDaTabela) {
        script_tabelas = "CREATE TABLE " + NomeDaTabela + " ( ";
    }

    public String script_tabelas;
    private boolean verif = false;
    public void Add_Atributo(String name, String tipagem) {
        if (script_tabelas.equals("")) throw new IllegalArgumentException("ILEGAL ARGUMENT EXCEPTION BEFORE CREATETABLE PLIS");

        if (!verif) {

            script_tabelas += " \n " + name + " " + tipagem +" " ;
            verif = true;
            return;
        }

        script_tabelas += ", \n " + name + " " + tipagem +" " ;

    }

    public String Build() {
        return script_tabelas += " );";
    }

}
