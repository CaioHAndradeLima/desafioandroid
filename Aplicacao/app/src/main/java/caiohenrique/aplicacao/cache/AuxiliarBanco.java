package caiohenrique.aplicacao.cache;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.renderscript.Script;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import caiohenrique.aplicacao.Repositorio;
import caiohenrique.aplicacao.owner;

/**
 * Created by Caio on 09/07/2017.
 */

public class AuxiliarBanco {

    private SQLiteDatabase db;
    private CreateDB banco;

    public AuxiliarBanco(Context c) {
        banco = new CreateDB(c);

    }

    public boolean insereUsuario(Repositorio repositorio) {
        ContentValues valores = returnContentValuesRepositorio(repositorio);
        long resultado;

        db = banco.getWritableDatabase();

        resultado = db.insertOrThrow(ScriptDeTabela.NAMETABLE, null, valores);
        db.close();

        return resultado != -1 ? true : false;
    }

    public ContentValues returnContentValuesRepositorio(Repositorio repositorio) {
        ContentValues values = new ContentValues();
        values.put(ScriptDeTabela.nome_1 , repositorio.getName() );
        values.put(ScriptDeTabela.full_name_2 , repositorio.getFull_name() );
        values.put(ScriptDeTabela.Description_3 , repositorio.getDescription() );
        values.put(ScriptDeTabela.Forks_4 , repositorio.getForks() );
        values.put(ScriptDeTabela.Owner_Login_5 , repositorio.Dono.getLogin() );
        values.put(ScriptDeTabela.Owner_COD_6 , repositorio.Dono.getId() );
        values.put(ScriptDeTabela.Owner_AvatarURL_7 , repositorio.Dono.getAvatar_url());
        return values;
    }


    public List<Repositorio> BuscaTodosRepositorios() throws JSONException {
        Repositorio repositorio = null;
        List<Repositorio> Lista = new ArrayList<>();
        db = banco.getReadableDatabase();
        Cursor cursor = db.query(ScriptDeTabela.NAMETABLE, null, null, null, null, null, null);
        if (cursor.getCount() > 0) { //se tiver dados na tabela
            cursor.moveToFirst(); // vai mover pro primeiro
            do {

                repositorio = new Repositorio();
                repositorio.Dono = new owner();
                repositorio.setId(cursor.getInt(0));
                repositorio.setName(cursor.getString(1));
                repositorio.setFull_name(cursor.getString(2));
                repositorio.setDescription(cursor.getString(3));
                repositorio.setForks(cursor.getString(4));
                repositorio.Dono.setLogin(cursor.getString(5));
                repositorio.Dono.setId(cursor.getString(6));
                repositorio.Dono.setAvatar_url(cursor.getString(7));
                Lista.add(repositorio);
            } while (cursor.moveToNext()); //Condição para parar de pegar, quando acabar os dados
        } else { db.close(); return null; }
        db.close();
        return Lista;
    }


}
