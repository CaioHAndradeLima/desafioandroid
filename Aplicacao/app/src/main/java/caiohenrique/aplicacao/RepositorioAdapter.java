package caiohenrique.aplicacao;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;

import caiohenrique.aplicacao.resources_http.DownloadImagem;
import caiohenrique.aplicacao.resources_http.GerenciadorHttp;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Caio on 08/07/2017.
 */

public class RepositorioAdapter  extends RecyclerView.Adapter<RepositorioAdapter.MyViewHolder> {
    private List<Repositorio> mList;
    private LayoutInflater mLayoutInflater;
    private Context context;
    private CliqueItem clique;


    public RepositorioAdapter(Context c, List<Repositorio> lista, CliqueItem clique){
        mList = lista;
        mLayoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = c;
        this.clique = clique;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = mLayoutInflater.inflate(R.layout.layout_lista_repositorios, viewGroup, false);
        MyViewHolder mvh = new MyViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, int position) {
        Log.i("LOG", "onBindViewHolder()");
        myViewHolder.titulo.setText(mList.get(position).getName() );
        myViewHolder.nome_criador.setText( mList.get(position).Dono.getLogin() );
        myViewHolder.forks.setText(mList.get(position).getForks());
        myViewHolder.descricao.setText(mList.get(position).getDescription());



        if(mList.get(position).Dono.getBitmap() == null) {
            new DownloadImagem(myViewHolder.imagem_perfil,mList.get(position),context).execute();
        } else {
            myViewHolder.imagem_perfil.setImageBitmap(mList.get(position).Dono.getBitmap());

        }




    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    public void addListItem(Repositorio c, int position){
        mList.add(c);
        notifyItemInserted(position);
    }


    public void removeListItem(int position){
        mList.remove(position);
        notifyItemRemoved(position);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public CircleImageView imagem_perfil;
        public TextView titulo, nome_criador, forks, descricao;

        public MyViewHolder(View itemView) {
            super(itemView);
            imagem_perfil = itemView.findViewById(R.id.layout_lista_repositorios_CircleImageView);
            titulo = itemView.findViewById(R.id.layout_lista_repositorios_titulo);
            nome_criador = itemView.findViewById(R.id.layout_lista_repositorios_Name_Usuario);
            forks = itemView.findViewById(R.id.layout_lista_repositorios_forks);
            descricao = itemView.findViewById(R.id.layout_lista_repositorios_Descricao_repositorio);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clique.Clique(getPosition());
        }
    }

    public interface CliqueItem {
        public void Clique(int position);
    }
}
