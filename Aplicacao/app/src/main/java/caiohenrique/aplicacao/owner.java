package caiohenrique.aplicacao;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.Serializable;

import caiohenrique.aplicacao.resources_http.GerenciadorHttp;

/**
 * Created by Caio on 08/07/2017.
 */

public class owner  implements Serializable {
    private String login, id, avatar_url;
    private Bitmap bitmap = null;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public void setImageBitmap(Bitmap bitmap, Context activity) {
        File file1 = GerenciadorHttp.CriarFileBitmap(activity, "imagemcortada.jpg", bitmap);
        this.bitmap = decodeFileAndRecort(file1, 250);
    }

    public Bitmap getBitmap() {
        return this.bitmap;
    }


    // Decodifica a imagem e escala para a redução do consumo de memória
    public static Bitmap decodeFileAndRecort(File f , int Required_Size) {
        try {
            // Decodifica o tamanho da imagem
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            // O novo tamanho que queremos
            final int REQUIRED_SIZE = Required_Size;



            // Achar o valor correto para a escala
            int scale = 1;
            while(o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            // Decodifica com o inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {}
        return null;
    }
}
